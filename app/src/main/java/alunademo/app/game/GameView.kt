package alunademo.app.game

import GameManager
import alunademo.app.R
import android.content.Context
import android.graphics.*
import android.support.v4.content.res.ResourcesCompat
import android.util.AttributeSet
import android.view.SurfaceHolder
import android.view.SurfaceView
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.util.*

fun getElapsedSeconds(from: Date, to:Date): Long {
    return (to.time - from.time)/1000
}

class GameView(context: Context, attrs: AttributeSet? = null) : SurfaceView(context, attrs),
    SurfaceHolder.Callback, AnkoLogger, Runnable, Helpers {

    // This is our thread
    private val gameThread = Thread(this)
    private var running = false
    private var fps = 0

    var gameType = "Lava"

    override fun surfaceChanged(p0: SurfaceHolder?, p1: Int, p2: Int, p3: Int) {
    }

    override fun surfaceDestroyed(p0: SurfaceHolder?) {
        running = false
    }

    private var cumFps = 0
    private var avgFps = 0
    private var startTime = Date()
    private var endTime = Date()

    private var leftMargin = 0

    init {
        holder.addCallback(this)
    }

    override fun surfaceCreated(p0: SurfaceHolder?) {
        setWillNotDraw(false)
        GameManager.init(toDp(width), toDp(height))
        leftMargin = width/13
        info("surfaceCreated phone size : $width  $height px ${toDp(width)} ${toDp(height)} dp")
        resume()

    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        density = context.resources.displayMetrics.density
        info("onLayout view size : $width  $height px ${toDp(width)} ${toDp(height)} dp")
    }

    override fun onDraw(canvas: Canvas) {

    }

    private fun drawAll(canvas: Canvas) {
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR)
        drawBitmaps(canvas)
        drawScores(canvas)
        drawCoords(canvas)
        drawFps(canvas)
    }

    protected fun drawWorld(canvas: Canvas) {
        var world : World? = null
        when (gameType) {
            "Quest" -> world = GameManager.questWorld
            "Lava" -> world = GameManager.lavaWorld
        }
        canvas.drawBitmap(world!!.bitmap, 0.toFloat(), toPx(world!!.viewY).toFloat(),null)
    }

    protected fun drawBitmaps(canvas: Canvas) {
//        canvas.drawBitmap(GameManager.backgroundBM, Rect(0, -GameManager.backgroundYScroll, width, height-GameManager.backgroundYScroll),
//            Rect(0, 0, width, height), null)
//        canvas.drawBitmap(GameManager.backgroundBM, 0.toFloat(), toPx(-GameManager.backgroundYScroll).toFloat(),null)
        drawWorld(canvas)
        for ((id, sprite) in GameManager.sprites) {
            when (sprite) {
                is BitmapSprite -> {
                    val bitmapSprite = sprite as BitmapSprite
//                    canvas.drawBitmap(bitmapSprite.bitmap, Rect(sprite.prevX, sprite.prevY, sprite.prevX+sprite.width, sprite.prevY+sprite.height),
//                        Rect(sprite.x, sprite.y, sprite.x+sprite.width, sprite.y+sprite.height), null)
                    canvas.drawBitmap(bitmapSprite.bitmap, toPx(sprite.x).toFloat(), toPx(sprite.y).toFloat(), null)
                }
            }
        }
    }

    private fun getPaint(color: Int, textSize: Float, font: Typeface?): Paint {
        val paint: Paint = Paint()
        paint.typeface = font
        paint.textSize = textSize
        paint.color = color
        return paint
    }

    protected fun drawFps(canvas: Canvas) {
        val paint: Paint = getPaint(Color.BLUE,
            60.toFloat(), null)
        canvas.drawText("Avg FPS = $avgFps", leftMargin.toFloat(), height.toFloat()/4.5.toFloat(), paint)
    }

    protected fun drawScores(canvas: Canvas) {
        val paint: Paint = getPaint(Color.argb(220, 160, 160, 230),
            220.toFloat(),
            ResourcesCompat.getFont(context, R.font.gameplay))
        val score = GameManager.score
        canvas.drawText("$score", leftMargin.toFloat(), height.toFloat()/8, paint)
        paint.textSize = 100.toFloat()
        paint.color = Color.rgb(200, 130, 130)
        canvas.drawText("FEV", leftMargin.toFloat(), height.toFloat()/5.5f, paint)
    }

    private fun drawCoords(canvas: Canvas) {
        val paint: Paint = getPaint(Color.BLACK,
            60.toFloat(), null)
        if(gameType == "Lava") {
            var sprite = GameManager.sprites[0]!!
            canvas.drawText(
                "$width $height ${toDp(width)} ${toDp(height)} Box ${sprite.x} ${sprite.y} ${sprite.x + sprite.width} ${sprite.y + sprite.height}",
                leftMargin.toFloat(), toPx(700).toFloat(), paint
            )
        }
    }

    private fun resume() {
        running = true
        when(gameType) {
            "Quest" -> GameManager.prepareQuestAssets(context)
            "Lava" -> GameManager.prepareLavaAssets(context)
        }
        gameThread.start()
    }

    override fun run() {

        var prevTime = Date()

        while (running) {

            val currentTime = Date()

            if ((currentTime.time-prevTime.time) > 30 && holder.surface.isValid) {
                // Lock the canvas ready to draw
                GameManager.update()
                var canvas: Canvas? = null
                if (running) {
                    try {
                        canvas = holder.lockCanvas()

                        if (canvas != null) {
                            drawAll(canvas)
                            fps++
                        }

                        // Draw everything to the screen
                    } finally {
                        if (running) {
                            holder.unlockCanvasAndPost(canvas)
//                            postInvalidate()
                        }
                    }
                }
            }
            if ( getElapsedSeconds(prevTime, currentTime) >= 1 ) {
                cumFps += fps
                fps = 0
                endTime = Date()
                avgFps = (cumFps.toLong()/ getElapsedSeconds(startTime, endTime)).toInt()
                prevTime = currentTime
            }
        }
    }



}
