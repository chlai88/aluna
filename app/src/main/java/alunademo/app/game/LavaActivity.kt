package alunademo.app.game

import GameManager
import alunademo.app.R
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_lava.*
import org.jetbrains.anko.AnkoLogger

class LavaActivity  : AppCompatActivity(), AnkoLogger {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lava)

        lavaLayout.setOnTouchListener(object : OnSwipeTouchListener(applicationContext) {

            override fun onSwipeTop(diffY: Int, velocity: Int) {
                super.onSwipeTop(diffY, velocity)
            }

            override fun onSwipeBottom(diffY: Int, velocity: Int) {
                super.onSwipeBottom(diffY, velocity)
            }

            override fun onSwipeLeft() {
                super.onSwipeLeft()
                cleanup()
            }

            override fun onSwipeRight() {
                super.onSwipeRight()
            }
        })

        lavaView.gameType = "Lava"
        GameManager.gameType = "Lava"

    }

    override fun onDestroy() {
        super.onDestroy()
        cleanup()
    }
}