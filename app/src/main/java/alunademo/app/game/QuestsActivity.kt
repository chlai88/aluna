package alunademo.app.game

import GameManager
import alunademo.app.R
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.widget.OverScroller
import kotlinx.android.synthetic.main.activity_game.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

class QuestsActivity() : AppCompatActivity(), AnkoLogger {

    private lateinit var scroller : OverScroller

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game)
        scroller = OverScroller(applicationContext)

        captain_button.setOnClickListener { view ->
            Snackbar.make(view, "Sorry not implemented", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        meter_button.setOnClickListener { view ->
            Snackbar.make(view, "Sorry not implemented", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        questLayout.setOnTouchListener(object : OnSwipeTouchListener(applicationContext) {

            override fun onSwipeTop(diffY: Int, velocity: Int) {
                super.onSwipeTop(diffY, velocity)
            }

            override fun onSwipeBottom(diffY: Int, velocity: Int) {
                super.onSwipeBottom(diffY, velocity)
            }

            override fun onSwipeLeft() {
                super.onSwipeLeft()
                cleanup()
            }

            override fun onSwipeRight() {
                super.onSwipeRight()
            }

            override fun onScrollTop(diff: Int) {
                GameManager.questWorld.moveView(0, diff)
            }

            override fun onScrollBottom(diff: Int) {
                GameManager.questWorld.moveView(0, -diff)
            }
        })

//        questLayout.setOnTouchListener(OverScrollerTouchListener(applicationContext))

        questView.gameType = "Quest"
        GameManager.gameType = "Quest"
    }

    override fun onDestroy() {
        super.onDestroy()
        cleanup()
    }


}

