package alunademo.app.game

import android.graphics.Bitmap

class World(val bitmap: Bitmap, val viewportWidth: Int, val viewportHeight: Int) : Helpers {

    val mapWidth = toDp(bitmap.width)
    val mapHeight = toDp(bitmap.height)

    var viewX = 0
    var viewY = 0

    fun moveView(x: Int, y:Int) {
        viewX += x
        viewY += y
    }
}