package alunademo.app.game

import alunademo.app.R
import android.support.v7.app.AppCompatActivity

var density: Float = 0f

fun AppCompatActivity.cleanup() {
    finish()
    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
}

interface Helpers {

    fun toPx(dp: Int) : Int {
        return (dp * density + 0.5).toInt()
    }

    fun toDp(px: Int) : Int {
        return (px / density - 0.5).toInt()
    }

}
