package alunademo.app.game

import android.content.Context
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info

open class OnSwipeTouchListener(ctx: Context) : View.OnTouchListener, AnkoLogger {

    private val gestureDetector: GestureDetector

    companion object {

        private val SWIPE_THRESHOLD = 100
        private val SWIPE_VELOCITY_THRESHOLD = 100
    }

    init {
        gestureDetector = GestureDetector(ctx, GestureListener())
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        info("OnSwipeTouchListener onTouch")
        return gestureDetector.onTouchEvent(event)
    }

    private inner class GestureListener : GestureDetector.SimpleOnGestureListener() {

        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
            var result = false
            try {
                val diffY = e2.y - e1.y
                val diffX = e2.x - e1.x
                if (Math.abs(diffX) > Math.abs(diffY)) {
                    if (Math.abs(diffX) > SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                        if (diffX > 0) {
                            onSwipeRight()
                        } else {
                            onSwipeLeft()
                        }
                        result = true
                    }
                } else if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY) > SWIPE_VELOCITY_THRESHOLD) {
                    if (diffY > 0) {
                        onSwipeBottom(Math.abs(diffY).toInt(), Math.abs(velocityY).toInt())
                    } else {
                        onSwipeTop(Math.abs(diffY).toInt(), Math.abs(velocityY).toInt())
                    }
                    result = true
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
            }

            return result
        }

        private var prevDown = 0
        private var prevMove = 0
        override fun onScroll(down: MotionEvent?, move: MotionEvent?,
                              distanceX: Float,
                              distanceY: Float
        ): Boolean {
            val diff = Math.abs(move!!.y - down!!.y)
            val prevDiff = Math.abs(prevMove - prevDown)
            if (diff > prevDiff) {
                if (move!!.y > down!!.y)
                    onScrollBottom((diff / 7).toInt())
                else
                    onScrollTop((diff / 7).toInt())

                prevMove = move.y.toInt()
                prevDown = down.y.toInt()
            }
            return super.onScroll(down, move, distanceX, distanceY)
        }
    }

    open fun onSwipeRight() {}

    open fun onSwipeLeft() {}

    open fun onSwipeTop(diffY: Int, velocity: Int) {}

    open fun onSwipeBottom(diffY: Int, velocity: Int) {}

    open fun onScrollTop(diff: Int) {}

    open fun onScrollBottom(diff: Int) {}

}
