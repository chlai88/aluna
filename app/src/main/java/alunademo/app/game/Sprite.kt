package alunademo.app.game

import android.graphics.Point
import android.graphics.Bitmap

interface Sprite<T> {

    var x : Int
    var y : Int
    var prevX: Int
    var prevY: Int
    var width : Int
    var height : Int
    var resource : T

    fun getLocation() : Point {
        return Point(x, y)
    }

    fun move(newX: Int, newY: Int) {
        prevX = x
        prevY = y
        x += newX
        y += newY
    }

    fun setLocation(newX: Int, newY:Int) {
        prevX = x
        prevY = y
        x = newX
        y = newY
    }

    fun setSize(w: Int, h: Int) {
        width = w
        height = h
    }
}

class BitmapSprite(val bitmap: Bitmap) : Sprite<Bitmap> {

    override var x = 0
    override var y = 0
    override var prevX = 0
    override var prevY = 0
    override var width = 0
    override var height = 0
    override var resource : Bitmap = bitmap

    init {
        setSize(bitmap.width, bitmap.height)
    }
}