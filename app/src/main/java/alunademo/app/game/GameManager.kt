import alunademo.app.R
import alunademo.app.game.BitmapSprite
import alunademo.app.game.Sprite
import alunademo.app.game.Helpers
import alunademo.app.game.World
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.util.*
import kotlin.random.Random

object GameManager : AnkoLogger, Helpers {

    private var ticks = 0
    private var scoreRefreshTicks = 0

    private var startTime = Date().time
    private var endTime = Date().time

    private var viewportHeight = 0
    private var viewportWidth = 0

    lateinit var questWorld : World
    lateinit var lavaWorld : World

    var gameType = "Quest"

    var sprites = mutableMapOf<Int, Sprite<*>>()

    var score: Int = Random.nextInt(60, 100)

    fun init(w: Int, h: Int) {
        viewportHeight = h
        viewportWidth = w
    }

    fun prepareQuestAssets(context: Context) {
        cleanup()
        info("prepareQuestAssets")
        lavaWorld = World( Bitmap.createScaledBitmap(
            BitmapFactory.decodeResource(
                context.resources,
                R.drawable.lava), toPx(viewportWidth), toPx(viewportHeight+500), false),
            viewportWidth, viewportHeight)
        questWorld = World( Bitmap.createScaledBitmap(
            BitmapFactory.decodeResource(
                context.resources,
                R.drawable.map_black), toPx(viewportWidth), toPx(viewportHeight+500), false),
            viewportWidth, viewportHeight)
        questWorld.moveView(0, -500)
        // The gem
        sprites[1] = BitmapSprite(Bitmap.createScaledBitmap(
            BitmapFactory.decodeResource(
                context.resources,
                R.drawable.gem), toPx(viewportWidth/4), toPx(viewportHeight/8), false))
        sprites[1]!!.setLocation(toPx((viewportWidth)-(sprites[1]!!.width)), toPx(viewportHeight/8))
        populateStars()
    }

    fun prepareLavaAssets(context: Context) {
        cleanup()
        questWorld = World( Bitmap.createScaledBitmap(
            BitmapFactory.decodeResource(
                context.resources,
                R.drawable.map_black), toPx(viewportWidth), toPx(viewportHeight+500), false),
            viewportWidth, viewportHeight)
        lavaWorld = World( Bitmap.createScaledBitmap(
            BitmapFactory.decodeResource(
                context.resources,
                R.drawable.lava), toPx(viewportWidth), toPx(viewportHeight+500), false),
            viewportWidth, viewportHeight)
        lavaWorld.moveView(0, -500)
        // The rocket ship
        sprites[0] = BitmapSprite(Bitmap.createScaledBitmap(
            BitmapFactory.decodeResource(
                context.resources,
                R.drawable.rocket), toPx(viewportWidth/4), toPx(viewportHeight/5), false))
        sprites[0]!!.setLocation(toPx((viewportWidth/2)-(sprites[0]!!.width/2)), toPx(viewportHeight-(viewportHeight/8)))
        sprites[0]!!.move(0, -700)
        // The gem
        sprites[1] = BitmapSprite(Bitmap.createScaledBitmap(
            BitmapFactory.decodeResource(
                context.resources,
                R.drawable.purple_gem), toPx(viewportWidth/4), toPx(viewportHeight/8), false))
        sprites[1]!!.setLocation(toPx((viewportWidth)-(sprites[1]!!.width)), toPx(viewportHeight/8))
        populateGems()
    }

    fun prepareAssets(context: Context) {
        when(gameType) {
            "Quest" -> prepareQuestAssets(context)
            "Lava" ->  prepareLavaAssets(context)
        }
    }

    fun populateGems() {
        sprites[1]!!.setLocation((viewportWidth)-(sprites[1]!!.width), viewportHeight/8)
        sprites[2] = BitmapSprite(sprites[1]!!.resource as Bitmap)
        sprites[3] = BitmapSprite(sprites[1]!!.resource as Bitmap)
        sprites[4] = BitmapSprite(sprites[1]!!.resource as Bitmap)
        for ((id, sprite) in sprites) {
            when {
                id > 1 -> sprite.setLocation(Random.nextInt(viewportWidth/2-100,viewportWidth/2+100), Random.nextInt(-800, 300))
            }
        }
    }

    fun populateStars() {
        sprites[1]!!.setLocation((viewportWidth)-(sprites[1]!!.width), viewportHeight/8)
        sprites[2] = BitmapSprite(sprites[1]!!.resource as Bitmap)
        sprites[3] = BitmapSprite(sprites[1]!!.resource as Bitmap)
        sprites[4] = BitmapSprite(sprites[1]!!.resource as Bitmap)
        for ((id, sprite) in sprites) {
            when {
                id > 1 -> sprite.setLocation(Random.nextInt(5, viewportWidth-30), Random.nextInt(2, viewportHeight))
            }
        }
    }

    fun getElapsed(): Long {
        val elapsed = endTime - startTime
        endTime = Date().time
        return elapsed
    }

    fun update() {


        if (gameType == "Quest") {
            val stars = mutableListOf<Sprite<*>>()
            for ((id, sprite) in sprites) {
                when {
                    id >= 1 -> stars.add(sprite)
                }
            }
            for (star in stars) {
                val dir = Random.nextInt(1, 4)
                when (dir) {
                    1 -> star.move(2, 0)
                    2 -> star.move(0, 2)
                    3 -> star.move(-2, 0)
                    4 -> star.move( 0, -2)
                }
            }
            for (star in stars) {
                if (star.y > viewportHeight) star.y = 20
                if (star.x > viewportWidth) star.x = 20
                if (star.x < 0) star.x = viewportWidth - 30
                if (star.y < 0) star.y = viewportHeight - 30
            }
        }

        if (gameType == "Lava") {
            val rocket = sprites[0]!!
            if (lavaWorld.viewY <= 50) {
                rocket.move(0, -2)
                lavaWorld.moveView(0, 1)
//                if (rocket.y > viewportHeight + 100 || rocket.y < 0) {
//                    rocket.y = viewportHeight
//                }
                val gems = mutableListOf<Sprite<*>>()
                for ((id, sprite) in sprites) {
                    when {
                        id >= 1 -> gems.add(sprite)
                    }
                }

                for (gem in gems) {
                    gem.move(0, 1)
                }
            }
        }


        if (scoreRefreshTicks++ > 100) {
            score = Random.nextInt(60, 100)
            scoreRefreshTicks = 0
        }
    }

    fun cleanup() {
        sprites.clear()
    }
}
